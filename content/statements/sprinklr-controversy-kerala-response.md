---
title: "Sprinklr controversy in Kerala and our response"
date: 2020-04-29T15:25:04+05:30
draft: false
---

> "**The Government has adopted the Free and Open source technologies as one of the basic guiding principles and shall strive for the promotion and adoption of the same. The Government shall make it mandatory for all software solutions made through public funding to adopt free and Open source technologies.**"
>>Kerala I.T Policy 2017 (Section 3.4)

Let's look at Free Software as a reminder. Free Software is a software which gives Freedom to use, study, modify and share and thus gives us control instead of being controlled by companies who restrict our Freedoms over the software. With Software playing an important role in our daily lives, lack of Freedoms in software affects our daily lives as well.

### Service as a Software Substitute:
SaaSS is a model of converting software traditionally run as software on our machines (personal or owned by organizations like govt or private companies) to service over the internet. With this change we are locked into these companies for accessing our own data or processed data generated from our input data. Examples of such services include Google Docs, Office 365, etc which allow creating and modifying documents like spreadsheets or presentations or a service like Azure Machine Learning Designer

### "Cloud" Services:
There is also a large number of web based services that do not have a traditional software, like a music player application, equivalent and clubbed together as "Cloud" services". Some examples are WhatsApp, Facebook, Youtube, Slack, etc. Many of the issues we talk about in the context of SaaSS are also important in this context.

> *Richard Stallman, who is the founder of Free Software Foundation, recommends using “Service as a Software Substitute” or “SaaSS” instead of its original term “SaaS” or “Software as a Service”*.

This can have various consequences to our privacy, security and access to our own data. The software used by these SaaSS providers can be Proprietary or Free Software.

### SaaSS using Proprietary Software:
SaaSS using Proprietary Software poses an even bigger threat than proprietary software, as in the traditional proprietary software model, we still owned and controlled data, with SaaSS using Proprietary Software, we lose control of both software as well as data.  We will not be able to audit the software and will have to blindly trust the service provider to keep our data secure and privacy policies are followed in practice.

SaaSS using Free Software: If SaaSS is powered by Free Software, we have a choice of service providers and an option of being our own provider by installing the Free Software in a server we have access to. If one service provider is not providing satisfactory service, we can take our data and move to another provider or setup our own service.
Fortunately, for many such software rented as a service, we have good replacements created by Free Software community. NextCloud is an example of such software which offers file sharing services similar to services like Google Drive or DropBox. Matrix is an example of such software offering instant messaging which can be used as a replacement for services like WhatsApp and Telegram. It also supports voice and video calls including conferences using Jitsi which can be used as a replacement for Skype, Zoom, etc.

While using a provider of above services, we need to keep in mind that privacy of shared data is not ensured just on the promise of free software being used to provide the service but on the commitment to privacy and track record of the company providing the service.

There are services like disroot.org, poddery.com, diasp.in which make access to services as easy as their proprietary counterparts. These are run by people who care about privacy and supported by users of these services.  When we have Free Software available for offering as a service, different companies can offer the same service with different terms and conditions, which gives us choice. When no option is satisfactory we can even start our own service. With Free Software and such community run infrastructure we can reclaim our privacy

Though this require us to support the people managing these services to make it sustainable over long term. Running these services cost money and efforts and each of us can be a part of it.

### Privacy:
We are forced to share our private data to access these services. When we are not paying for a service, we or our data may be the service they are selling to advertising companies and governments. This can either be raw data that can be sold as is or processed data that can be used for advanced profiling and personalized advertising. For example in 2017, Cambridge Analytica claimed that it has psychological profiles of 240 million United States citizens, based on 5,000 separate data sets <sup>[1]</sup>.

The main data set for building this profiles were largely sourced from micro profiling done by Facebook and provided as a service<sup>[2] [3] [4]</sup>. This was then combined with various datasets that are sold by data brokers, like Axciom, Epsilon or Experian who aggregate all kinds of data, to build an advanced profile<sup>[5]</sup>. Facebook is just one of the biggest profilers of data which should be private and other big profilers in the list include Google<sup>[6] [7]</sup>, Amazon <sup>[8] [9] [10]</sup>, Microsoft <sup>[11]</sup> and Apple <sup>[12] [13] [14]</sup>. Jio<sup>[15]</sup> services is another example of big profiler of data if we are considering India alone who now have Facebook as a big shareholder. This helps to understand the level of profiling done that is used to manipulate us.

Manipulation can range from highly targeted advertising focused at you to prompt you to buy a product or building a bubble around the information you see online limiting your agency of choice in making a decision since you have partial/biased information to judge. The more someone knows about us, especially companies or governments with sole profit or power motives, the more power they can have over us and thus make us more vulnerable to manipulation and exploitation. Privacy sets limits on such power governments and companies can have over us.

### Access to our own data and software:
SaaSS and cloud services usually provide security from external threats because otherwise it affects their reputation. While it may seem convenient it comes with the price of true ownership and control over our own data and software. One recent example for the consequences of not having necessary control on our own data and software is Adobe shutting down access to all their services to people of Venezuela after USA declared sanctions against Venezuela <sup>[16]</sup>. This meant people of Venezuela were locked out of their own data, purchased licensed software and were given a small window to download their data. With most of the data stored in non-free formats, it is not easy to switch to other software to access the data (Many Free Software applications reverse engineer such non-free formats and offer accessing such data, but compatibility varies for different formats). Another example is where Google prohibitting Google apps from working on newly launched Android devices in Turkey, after the Turkish Competition Board ruled that changes Google made to its contracts were not acceptable<sup>[17]</sup>.

### Government vs Individuals:
As an individual each person may value privacy differently and make choices about using these services. But a government can't make these choices lightly because they are constutionally mandated to protect everyone's fundamental rights including privacy. When entering into agreements to share data with companies, the terms and limits of data usage must be set by the government instead of allowing the companies to decide these policies which are usually designed to exploit people's data for profit. A company's privacy policy usually do not describe what happens when a data theft occurs, or what happens when a acquisition happens. The intention of a company is always protecting its stakeholders and profit. The intention of a government, by the people, should always be to protect the interests of its people. This is where the government policy regarding sprinklr has failed. 

It is not right to say that this should not be discussed at the time of an epidemic, because privacy of an individual, once lost is lost for ever. That means his right to speech, right to not to be discriminated, and other basic rights disappear as well. Some justify the data sharing with Sprinklr by Kerala government to manage COVID-19 contact list arguing their data is of no value or they have nothing to hide <sup>[18] [19]</sup>. Edward Snowden's words gives us some insight into how shallow that line of argument is, *“Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say.”*

This Sprinklr incident should be converted to an opportunity to discuss various aspects of this issue in detail and arrive at solutions in the long term. As with Kerala model in many other areas, we should lead in governance of personal data as well.

### Call to action - Data Protection Law and Free Software Use:
Government should form laws and ensure that they're strictly followed to protect the data of the citizens. The laws should clearly define the rights and responsibilities. People should have the right to know what information the service providers are collecting and how it’s going to be used. No data should be collected without informed consent. People should have options to ask the provider to delete the collected data. It is sad to see a government leaning towards communist ideology falling prey to surveillance capitalism <sup>[20]</sup>. Our intention is to right the wrong done. In order to do that we recommend goverment and citizens to actively participate in this move to achieve self-reliance in digital services and infrastructure. Government could invest in more Free Software based solutions that can be self hosted and encourage citizens to use and support more community owned communication infrastructure.

There is now an offer to help made on 19th of April from an Indian company, Frappe, who provide the same service as Sprinklr using their Free Software solution - ErpNext <sup>[21]</sup>. The top stock broker  in India, Zerodha, uses ErpNext for its daily business. To put its capability into perspective, Zerodha has 2 million customers installed on ErpNext, processes 4-5 million database records per day and has a total of around 1.5 billion Ledger entries and [9 billion processed invoice items <sup>[22]</sup>.

We would like government to revaluate their decision to use Sprinklr and compare it to the Free Software CRM solution offered and explain the decision to use Sprinklr, if any. Primary argument of the  government has been that we lack technical know-how to implement this solution on our own. At this point we would also like to bring forth attention towards ICFOSS, an organisation set up by Kerala government to popularise Free software, whose budget runs into crores. Shouldn't this organization be able to evaluate and provide an advice on using a Free Software solution to the government?</p>

This is the perfect time to start thinking about these issues and on how to protect citizen's right to privacy. Our statement hopes to bring about a step in that direction.

### References:

1: https://www.bbc.com/news/technology-46822439

2: https://www.theguardian.com/technology/2018/mar/17/facebook-cambridge-analytica-kogan-data-algorithm

3: https://gizmodo.com/how-facebook-figures-out-everyone-youve-ever-met-1819822691?IR=T

4: https://www.techrepublic.com/article/facebook-data-privacy-scandal-a-cheat-sheet

5: https://techcrunch.com/2018/06/06/cambridge-analyticas-nix-said-it-licensed-millions-of-data-points-from-axciom-experian-infogroup-to-target-us-voters/

6: https://en.wikipedia.org/wiki/Privacy_concerns_regarding_Google

7: https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy

8: https://www.theatlantic.com/technology/archive/2018/10/amazon-selling-machine/574045/

9: https://theintercept.com/2019/11/26/amazon-ring-home-security-facial-recognition/

10: https://www.theguardian.com/technology/2019/dec/28/tech-industry-year-in-review-facebook-google-amazon

11: https://www.theguardian.com/world/2013/jul/11/microsoft-nsa-collaboration-user-data

12: https://www.forbes.com/sites/jeanbaptiste/2019/07/30/confirmed-apple-caught-in-siri-privacy-scandal-let-contractors-listen-to-private-voice-recordings

13: https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data

14: https://www.washingtonpost.com/investigations/us-intelligence-mining-data-from-nine-us-internet-companies-in-broad-secret-program/2013/06/06/3a0c0da8-cebf-11e2-8845-d970ccb04497_story.html

15: https://www.huffingtonpost.in/entry/reliance-jio-paytm-tell-trai-the-government-should-be-able-to-access-your-data_in_5c4f30e9e4b0f43e4109647c

16: https://www.theverge.com/2019/10/7/20904030/adobe-venezuela-photoshop-behance-us-sanctions

17: https://www.engadget.com/2019-12-17-android-turkey-google-apps.html

18: https://www.thenewsminute.com/article/explained-allegations-against-kerala-govt-privacy-breach-covid-19-patients-122559

19: https://www.livelaw.in/news-updates/plea-in-hc-challenges-kerala-govt-contract-with-us-company-sprinkler-to-process-covid-19-data-155451" 

20: https://www.theguardian.com/technology/2019/jan/20/shoshana-zuboff-age-of-surveillance-capitalism-google-facebook

21: https://medium.com/@rushabh_mehta/covid-case-tracking-using-foss-9e4a2168ec3a 

22: https://erpnext.com/customer-story/zerodha



[1]: https://www.bbc.com/news/technology-46822439

[2]: https://www.theguardian.com/technology/2018/mar/17/facebook-cambridge-analytica-kogan-data-algorithm

[3]: https://gizmodo.com/how-facebook-figures-out-everyone-youve-ever-met-1819822691?IR=T

[4]: https://www.techrepublic.com/article/facebook-data-privacy-scandal-a-cheat-sheet

[5]: https://techcrunch.com/2018/06/06/cambridge-analyticas-nix-said-it-licensed-millions-of-data-points-from-axciom-experian-infogroup-to-target-us-voters/

[6]: https://en.wikipedia.org/wiki/Privacy_concerns_regarding_Google

[7]: https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy

[8]: https://www.theatlantic.com/technology/archive/2018/10/amazon-selling-machine/574045/

[9]: https://theintercept.com/2019/11/26/amazon-ring-home-security-facial-recognition/

[10]: https://www.theguardian.com/technology/2019/dec/28/tech-industry-year-in-review-facebook-google-amazon

[11]: https://www.theguardian.com/world/2013/jul/11/microsoft-nsa-collaboration-user-data

[12]: https://www.forbes.com/sites/jeanbaptiste/2019/07/30/confirmed-apple-caught-in-siri-privacy-scandal-let-contractors-listen-to-private-voice-recordings

[13]: https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data

[14]: https://www.washingtonpost.com/investigations/us-intelligence-mining-data-from-nine-us-internet-companies-in-broad-secret-program/2013/06/06/3a0c0da8-cebf-11e2-8845-d970ccb04497_story.html

[15]: https://www.huffingtonpost.in/entry/reliance-jio-paytm-tell-trai-the-government-should-be-able-to-access-your-data_in_5c4f30e9e4b0f43e4109647c

[16]: https://www.theverge.com/2019/10/7/20904030/adobe-venezuela-photoshop-behance-us-sanctions

[17]:https://www.engadget.com/2019-12-17-android-turkey-google-apps.html

[18]: https://www.thenewsminute.com/article/explained-allegations-against-kerala-govt-privacy-breach-covid-19-patients-122559

[19]: https://www.livelaw.in/news-updates/plea-in-hc-challenges-kerala-govt-contract-with-us-company-sprinkler-to-process-covid-19-data-155451" 

[20]: https://www.theguardian.com/technology/2019/jan/20/shoshana-zuboff-age-of-surveillance-capitalism-google-facebook

[21]: https://medium.com/@rushabh_mehta/covid-case-tracking-using-foss-9e4a2168ec3a 

[22]: https://erpnext.com/customer-story/zerodha

See the original thread on [codema.in](https://codema.in/d/fp7Q23sD/sprinklr-controversy-in-kerala-and-our-response) for discussions realted to this statement.